﻿using UnityEngine;
using System.Collections;

public class CheckLanguage : MonoBehaviour
{

    public static string GetLanguage()
    {
        string lang = Application.systemLanguage.ToString();
        switch (lang)
        {
            case "Polish": LocalizationText.SetLanguage("PL"); return lang;
            default: LocalizationText.SetLanguage("EN"); return "English";
        }
    }
}
