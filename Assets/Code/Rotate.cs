﻿using UnityEngine;

public class Rotate : MonoBehaviour
{
    Transform arrow;
    Transform north;

    bool globeSet = true;
    public bool startTimer = false;
    public Camera cam;
    public LayerMask Layer_Mask;

    void Start()
    {
        north = GameObject.FindGameObjectWithTag("North").transform;
    }

    void Update()
    {
        CheckIfSet();
        if (!globeSet && arrow != null) //if capital is already created set globe rotation
        {
            var zy = Mathf.Atan2(arrow.position.z, arrow.position.y) * 180f / Mathf.PI + 90f;
            var xz = Mathf.Atan2(arrow.position.x, arrow.position.z) * 180f / Mathf.PI;
            var north_zy = Mathf.Atan2(north.position.z, north.position.y) * 180f / Mathf.PI + 90f;

            if (Mathf.Abs(zy - 180f) > 1f || Mathf.Abs(xz) > 1f || Mathf.Abs(north_zy - 180f) > 1f)
            {
                transform.Rotate(new Vector3(-zy + 180f, 0f, 0f) * Time.deltaTime, Space.World);
                transform.Rotate(new Vector3(0f, -xz, 0f) * Time.deltaTime, Space.World);

                var north_yx = -Mathf.Atan2(north.position.y, north.position.x) * 180f / 3.1415f + 90f;
                transform.Rotate(new Vector3(0f, 0f, north_yx) * Time.deltaTime, Space.World);
            }
            else
            {
                globeSet = true;
            }
        }
        
    }

    public void findNewDot()
    {
        arrow = GameObject.FindGameObjectWithTag("Arrow").transform;  //Find capital
        startTimer = false;
        globeSet = false;
    }
    public void CheckIfSet()
    {
        Ray r = cam.ScreenPointToRay(new Vector2(Screen.width / 2f, Screen.height / 2f));
        RaycastHit hit;
        
        if(Physics.Raycast(r,out hit, Mathf.Infinity, Layer_Mask))
        {
            if (arrow != null)
            {
                if (Vector3.Distance(hit.point, arrow.transform.position) < .5f)
                {
                    startTimer = true;
                }
                else
                    startTimer = false;
            }
           
        }
    }
   
}
