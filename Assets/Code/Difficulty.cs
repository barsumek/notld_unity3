﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Difficulty : MonoBehaviour
{
	private int currentDifficulty = 0;
    private Image button;
    private Text buttonText;
    public Color easy;
    public Color medium;
    public Color hard;

    void Start()
    {
        currentDifficulty = PlayerPrefs.GetInt("Difficulty", 0);

        button = GetComponent<Image>();
        buttonText = button.gameObject.GetComponentInChildren<Text>();
    }

    public int CurrentDifficulty
    {
        get
        {
            return currentDifficulty;
        }

        set
        {
            currentDifficulty = value;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (UIManager.Instance.currentState == SceneState.Options)
        {
            switch (currentDifficulty)
            {
                case 0:
                    button.CrossFadeColor(easy, 1f, true, true);
                    buttonText.text = LocalizationText.GetText("opt-easy"); break;
                case 1:
                    button.CrossFadeColor(medium, 1f, true, true);
                    buttonText.text =LocalizationText.GetText("opt-medium"); break;
                case 2:
                    button.CrossFadeColor(hard, 1f, true, true);
                    buttonText.text = LocalizationText.GetText("opt-hard"); break;
            }
        }
	    
	}
    public void OnClick()
    {
        currentDifficulty++;
        currentDifficulty %= 3;
        PlayerPrefs.SetInt("Difficulty", currentDifficulty);
    }
}

