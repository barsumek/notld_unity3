﻿using UnityEngine;
using UnityEngine.UI;
[RequireComponent (typeof(Text))]
public class LoadTranslatedText : MonoBehaviour
{
    private Text textComponent;
    private string tempText;
    void Awake()
    {
        textComponent = GetComponent<Text>();
        textComponent.text = LocalizationText.GetText(textComponent.text);
    }
}
