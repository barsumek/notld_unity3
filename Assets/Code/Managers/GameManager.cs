﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class GameManager : Singleton<GameManager>
{
    protected GameManager() { }
    public GameObject indicator;
    public UIGame uiGame;
    //Stores the reference of a created dot in order to destroy it later
    private GameObject dot;
    //Reference of a globe to rotate
    private GameObject globe;
    private Quaternion globeOrigin;
    private Rotate rotateScript;
    public bool isTimeAttack = false;

    //List of all questions to pick from.
    private Dictionary<string, List<Place>> loadedQuestions;
    //List of questions chosen by the player.
    private Dictionary<string, List<Place>> questions;
    //List of questions for competetive mode
    private Dictionary<string, List<Place>> questions_competetive;

    //continents enabled in the options menu
    private List<string> enabledContinents;
    //continents for competetive mode
    private List<string> enabledContinents_competetive;
    //difficulty chosen in the options menu
    private int difficulty;
    public bool isCapitalsMode = true;


    void Awake()
    {
        //Initializing variables
        enabledContinents = new List<string>();
        enabledContinents_competetive = new List<string>();
        loadedQuestions = PlaceContainer.Load();
        ApplyOptions();
        
        globe = GameObject.FindGameObjectWithTag("Globe");
        globeOrigin = globe.transform.rotation;
        rotateScript = globe.GetComponent<Rotate>();
              
    }

    public void ApplyOptions()
    {
        enabledContinents.Clear();
        enabledContinents_competetive.Clear();
        difficulty = PlayerPrefs.GetInt("Difficulty", 0) + 1; //xml has difficulties from 1 to 3
        questions = new Dictionary<string, List<Place>>(loadedQuestions);
        questions_competetive = new Dictionary<string, List<Place>>(loadedQuestions);

        //add to the list all continents chosen by the player
        foreach (string continent in questions.Keys)
        {
            if (PlayerPrefs.GetInt(continent, 1) == 1)
            {
                enabledContinents.Add(continent);
            }
            enabledContinents_competetive.Add(continent);
        }

        //remove questions with difficulty higher than chosen by the player
        foreach (List<Place> places in questions.Values)
        {
            places.RemoveAll(place => place.Difficulty > difficulty);
        }
    }

    public void GetRandomDot()
    {
        //Index of a randomly generated question
        int questionIndex;

        //Name of a randomly generated continent to choose question from
        string randomContinent;
        int continentIndex;
        List<string> temp_continents;
        Dictionary<string, List<Place>> temp_questions;
        if (isTimeAttack)
        {
            temp_continents = new List<string>(enabledContinents_competetive);
            temp_questions = new Dictionary<string, List<Place>>(questions_competetive);
        }
        else
        {
            temp_continents = new List<string>(enabledContinents);
            temp_questions = new Dictionary<string, List<Place>>(questions);
        }
        continentIndex = UnityEngine.Random.Range(0, temp_continents.Count);
        randomContinent = temp_continents[continentIndex];
		questionIndex = UnityEngine.Random.Range(0, temp_questions[randomContinent].Count);
        //Current rotation
        Quaternion tempQuat = globe.transform.rotation;
        //Set the globe rotation to original rotation
        globe.transform.rotation = globeOrigin;

        //Set the position of the dot
		Vector3 dotPostion = new Vector3(temp_questions[randomContinent][questionIndex].Xpos,
			temp_questions[randomContinent][questionIndex].Ypos,
			temp_questions[randomContinent][questionIndex].Zpos);

        //Instantiate the random dot and set globe as its parent
        dot = (GameObject)Instantiate(
            indicator,
            dotPostion,
            Quaternion.identity);

        dot.transform.parent = globe.transform;
        dot.transform.localPosition = dotPostion;
        //Switch back to the previous rotation
        globe.transform.rotation = tempQuat;
        rotateScript.findNewDot();

        //calculate points to add in Time Rush attack
        int points = temp_questions[randomContinent][questionIndex].Difficulty * temp_continents.Count;

        //Generates all the possible answers based on index of question(dot)
        uiGame.GenerateAllAnswers(questionIndex, temp_questions, randomContinent, isCapitalsMode, points);

    }

    public void DestroyDot(float time)
    {
        Destroy(dot, time);
    }

}
