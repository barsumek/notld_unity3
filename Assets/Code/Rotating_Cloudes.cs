﻿using UnityEngine;
using System.Collections;

public class Rotating_Cloudes : MonoBehaviour
{
    public float RotSpeed;

	void Update()
    {
        transform.Rotate(new Vector3(0, Time.deltaTime * 4, (Mathf.Sin(Time.fixedTime) * Time.deltaTime) * 2) * RotSpeed);
	}

}