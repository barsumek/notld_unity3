﻿using UnityEngine;
using System.Collections;

public class PinchZoom : MonoBehaviour
{

    public float minFov = 7;
    public float maxFov = 35;
    public float zoomSpeed;
    private Camera cam;


    void Awake()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount == 1 && UIManager.Instance.currentState == SceneState.Game)
            {
            //    Touch t1 = Input.GetTouch(0);
            //    Touch t2 = Input.GetTouch(1);

            //    //Position of both touches in previous frame

            //    Vector2 prevt1 = t1.position - t1.deltaPosition;
            //    Vector2 prevt2 = t2.position - t2.deltaPosition;

            //    //Delta distance of touches in current and previous

            //    float touchDistance = (t2.position - t1.position).magnitude;
            //    float prevTouchDistance = (prevt2 - prevt1).magnitude;

            //    //How the delta distance changed, if increased then zoom in,
            //    //if decreased zoom out

            //    float deltaTouchDistance = prevTouchDistance - touchDistance;
            Touch t1 = Input.GetTouch(0);
            Vector2 prevt1=t1.position-t1.deltaPosition;
            
            

            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, cam.fieldOfView + prevt1.magnitude * zoomSpeed, zoomSpeed * Time.deltaTime);

            //Clamp values of the fov
            cam.fieldOfView = Mathf.Clamp(cam.fieldOfView, minFov, maxFov);


        }

    }
}
