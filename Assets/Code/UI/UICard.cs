﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UICard : MonoBehaviour {
    //GameObject associated with canvas UI card
    public GameObject canvasGameObject;

    public void FadeIn(float delayTime,float fadeDuration)
    {
        StartCoroutine(FadeCanvas(canvasGameObject, 1.0f, 1.0f, fadeDuration,delayTime, true));
    }
    public void FadeOut(float delayTime,float fadeDuration)
    {
        StartCoroutine(FadeCanvas(canvasGameObject, 0, 0, fadeDuration,delayTime, false));
    }

    /// <summary>
    /// IEnumerator which fades all canvas text and images
    /// </summary>
    /// <param name="canvas"></param>
    /// <param name="ImageAlpha"></param>
    /// <param name="TextAlpha"></param>
    /// <param name="fadeIn"> True if you want to fade in,false otherwise</param>
    /// <returns></returns>
    private IEnumerator FadeCanvas(GameObject GOcanvas, float ImageAlpha, float TextAlpha, float fadeDuration,float delayTime, bool fadeIn)
    {
        UIManager.Instance.cg.interactable = false;
        yield return new WaitForSeconds(delayTime);
        if (fadeIn)
            GOcanvas.SetActive(true);
        Canvas canvas = GOcanvas.GetComponent<Canvas>();
        Image[] imageList = canvas.GetComponentsInChildren<Image>();
        Text[] textList = canvas.GetComponentsInChildren<Text>();
        foreach (Image img in imageList)
        {
            if (fadeIn)
            {
                if (img.name == "Checkmark")
                {
                    if (img.gameObject.GetComponentInParent<Toggle>().isOn)
                    {
                        img.canvasRenderer.SetAlpha(0.02f);
                        img.CrossFadeAlpha(ImageAlpha, fadeDuration, false);
                    }
                }
                else
                {
                    img.canvasRenderer.SetAlpha(0.01f);
                    img.CrossFadeAlpha(ImageAlpha, fadeDuration, false);
                }
            }
            else
                img.CrossFadeAlpha(ImageAlpha, fadeDuration, false);
        }

        foreach (Text txt in textList)
        {
            if (fadeIn)
                txt.canvasRenderer.SetAlpha(0.02f);
            txt.CrossFadeAlpha(TextAlpha, fadeDuration, false);
        }

        yield return new WaitForSeconds(fadeDuration);
        UIManager.Instance.cg.interactable = true;
        if (fadeIn)
            GOcanvas.SetActive(true);
        else
            GOcanvas.SetActive(false);
        

    }
}
