﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class UIGameOver : MonoBehaviour
{
    public Button continue_btn;
    public void Retry()
    {
        continue_btn.interactable = true;
        UIManager.Instance.game.TurnButtonsOn();
        UIManager.Instance.game.HideGameOver();
        UIManager.Instance.currentState = SceneState.InitTimeAttack;
    }
    public void Exit()
    {
        continue_btn.interactable = true;
        UIManager.Instance.game.TurnButtonsOn();
        UIManager.Instance.game.HideGameOver();
        UIManager.Instance.FromGameToPlaymodes();
    }
    public void Continue()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                StartCoroutine(UIManager.Instance.game.AdErrorFun());
                continue_btn.interactable = false;
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
