﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIGame : UICard
{
    public List<Text> answerSlotsText;
    public List<Image> answerSlots;
    public Image[] errorList;
    public Text scorePractice;
    public Image timerImg;
    public Rotate rotator;
    public Text animText;
    public Text scoreRush;
    public Text highScoreText;
    public GameObject gameOverPanel;

    //Index of a randomly chosen answer (A,B,C,D) which will display correct answer
    private int correctAnswerSlotIndex;
    //Used to block user input for a while after clicking the button
    private bool clicked = false;
    //Used for statistics
    private int correctAnswers = 0;
    private int allAnswers = 0;

    private int rushPoints = 0;
    private int errors = 0;
    private int rushPointsToAdd;
    private float elapsed = 0;
    private bool startTimer = false;
    private float speed = 160f;
    private string answerTip;

    void CheckTheAnswerPractice(int index)
    {
        allAnswers++;
        //Play the sound
        if (index == correctAnswerSlotIndex)
        {
            AudioManager.instance.PlaySingle(AudioManager.instance.clips[0]);
            correctAnswers++;
        }
        else
        {
            AudioManager.instance.PlaySingle(AudioManager.instance.clips[1]);
        }
        scorePractice.text = LocalizationText.GetText("ui-correctAnswers") + correctAnswers.ToString() + "/" +
                             allAnswers.ToString();
    }

    void CheckTheAnswerRush(int index)
    {
        //Play the sound
        if (index == correctAnswerSlotIndex)
        {
            AudioManager.instance.PlaySingle(AudioManager.instance.clips[0]);
            rushPoints += rushPointsToAdd;
        }
        else
        {
            AudioManager.instance.PlaySingle(AudioManager.instance.clips[1]);
            errorList[errors].GetComponentInChildren<Animator>().SetTrigger("cross");
            errors++;
            if (errors == 3)
            {
                CheckHighScore();
                ResetTimer();
                gameOverPanel.GetComponent<Animator>().SetTrigger("show");
                FadeOutAnswers();
            }
        }
        scoreRush.text = rushPoints.ToString();
    }

    IEnumerator ColorAnswers()
    {
        animText.text = answerTip;
        animText.GetComponent<Animator>().SetTrigger("slide");
        clicked = true;
        //DestroyDot();

        //Color the images
        foreach (Image img in answerSlots)
        {
            Image[] imgs = img.gameObject.GetComponentsInChildren<Image>();
            if (answerSlots.IndexOf(img) == correctAnswerSlotIndex)
            {
                imgs[1].enabled = true;
            }
            else
            {
                imgs[2].enabled = true;
            }
        }
        GameManager.Instance.DestroyDot(1);
        yield return new WaitForSeconds(1.5f);

        //Reset colors
        foreach (Image img in answerSlots)
        {
            Image[] imgs = img.gameObject.GetComponentsInChildren<Image>();
            imgs[1].enabled = false;
            imgs[2].enabled = false;
        }
        if (GameManager.Instance.isTimeAttack)
        {
            if (errors != 3)
                GameManager.Instance.GetRandomDot(); //Generate new question
        }
        else
            GameManager.Instance.GetRandomDot();
        clicked = false;
        ResetTimer();
        yield return new WaitUntil(() => rotator.startTimer == true);
        if (GameManager.Instance.isTimeAttack && rotator.startTimer && errors != 3)
        {
            StartTimer();
        }
        yield return null;
    }

    public void GenerateAllAnswers(int questionIndex, Dictionary<string, List<Place>> questions, string continentName,
        bool isCapitalsMode, int points)
    {
        List<string> signs = new List<string>()
        {"A. ", "B. ", "C. ", "D. "};
        int tempIndex;

        //Select correct answer slot
        correctAnswerSlotIndex = UnityEngine.Random.Range(0, 4);
        //List needed for handling repeating answers
        List<int> answers = new List<int>();
        //Add the correct answer to the list
        answers.Add(questionIndex);

        //store points to add for Time Rush mode
        rushPointsToAdd = points;

        //Iterate through all answer slots
        foreach (Text answ in answerSlotsText)
        {
            int index = answerSlotsText.IndexOf(answ);
            //Check if our slot is supposed to display correct answer
            if (index == correctAnswerSlotIndex)
            {
                tempIndex = questionIndex;
                if (isCapitalsMode)
                {
                    answerTip = questions[continentName][tempIndex].Country;
                }
                else
                {
                    answerTip = questions[continentName][tempIndex].Capital;
                }
            }
            //Otherwise select random incorrect answer
            else
            {
                tempIndex = UnityEngine.Random.Range(0, questions[continentName].Count);
                while (answers.Contains(tempIndex))
                {
                    tempIndex = UnityEngine.Random.Range(0, questions[continentName].Count);
                }
            }
            //Update text and add the answer to list to avoid repeating
            if (isCapitalsMode)
            {
                answ.text = signs[index] + questions[continentName][tempIndex].Capital;
            }
            else
            {
                answ.text = signs[index] + questions[continentName][tempIndex].Country;
            }

            answers.Add(tempIndex);
        }
    }

    public void OnClick(int index)
    {
        if (!clicked)
        {
            if (GameManager.Instance.isTimeAttack)
            {
                CheckTheAnswerRush(index);
            }
            else
            {
                CheckTheAnswerPractice(index);
            }
            StartCoroutine(ColorAnswers());
            startTimer = false;
        }
    }

    public void StartTimer()
    {
        ResetTimer();
        startTimer = true;
    }

    public void ResetTimer()
    {
        startTimer = false;
        timerImg.rectTransform.sizeDelta = new Vector2(0, 0);
        elapsed = 0;
    }

    void Update()
    {
        if (startTimer && timerImg.rectTransform.sizeDelta.x <= 800)
        {
            elapsed += Time.deltaTime;
            timerImg.rectTransform.sizeDelta = new Vector2(elapsed*speed, 22);
        }
        else if (elapsed >= 5)
        {
            StartCoroutine(ColorAnswers());
            elapsed = 0;
            CheckTheAnswerRush(-1);
        }
        else
        {
            startTimer = false;
        }
    }

    public void ClearScores()
    {
        correctAnswers = 0;
        allAnswers = 0;
        scorePractice.text = LocalizationText.GetText("ui-correctAnswers") + "0/0";
        if (GameManager.Instance.isTimeAttack)
        {
            errors = 0;
            rushPoints = 0;
            scoreRush.text = rushPoints.ToString();

            HideGameOver();
        }
    }

    public void HideGameOver()
    {
        foreach (Image img in errorList)
        {
            //if (img.GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).IsName("cros"))
            img.GetComponentInChildren<Animator>().SetTrigger("clear");
        }

        if (gameOverPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("ShowGameOverPanel"))
        {
            gameOverPanel.GetComponent<Animator>().SetTrigger("hide");
        }
    }

    public void Back()
    {
        TurnButtonsOn();
        UIManager.Instance.FromGameToPlaymodes();
    }

    public IEnumerator AdErrorFun()
    {
        errors -= 2;
        errorList[2].GetComponentInChildren<Animator>().SetTrigger("clear");
        errorList[1].GetComponentInChildren<Animator>().SetTrigger("clear");
        GameManager.Instance.DestroyDot(0);
        GameManager.Instance.GetRandomDot();
        TurnButtonsOn();
        FadeInAnswers();
        gameOverPanel.GetComponent<Animator>().SetTrigger("hide");
        gameOverPanel.GetComponent<Animator>().SetTrigger("clear");
        yield return new WaitUntil(() => rotator.startTimer == true);
        if (rotator.startTimer)
        {
            startTimer = true;
        }
        yield return null;
    }

    public void FadeOutAnswers()
    {
        TurnButtonsOff();
        foreach (Image img in answerSlots)
        {
            img.CrossFadeAlpha(0, 1, false);
            foreach (Image childImage in img.gameObject.GetComponentsInChildren<Image>())
            {
                childImage.CrossFadeAlpha(0, 1, false);
            }
        }
        foreach (Text txt in answerSlotsText)
        {
            txt.CrossFadeAlpha(0, 1, false);
        }
    }

    public void FadeInAnswers()
    {
        TurnButtonsOn();
        foreach (Image img in answerSlots)
        {
            img.canvasRenderer.SetAlpha(0.02f);
            img.CrossFadeAlpha(1, 1, false);
            foreach (Image childImage in img.gameObject.GetComponentsInChildren<Image>())
            {
                childImage.CrossFadeAlpha(1, 1, false);
            }
        }
        foreach (Text txt in answerSlotsText)
        {
            txt.canvasRenderer.SetAlpha(0.02f);
            txt.CrossFadeAlpha(1, 1, false);
        }
    }

    public void TurnButtonsOn()
    {
        foreach (Image img in answerSlots)
        {
            img.GetComponent<Button>().interactable = true;
        }
    }

    public void TurnButtonsOff()
    {
        foreach (Image img in answerSlots)
        {
            img.GetComponent<Button>().interactable = false;
        }
    }

    void CheckHighScore()
    {
        if (GameManager.Instance.isCapitalsMode)
        {
            if (rushPoints > PlayerPrefs.GetInt("high_score_capitals", 0))
            {
                PlayerPrefs.SetInt("high_score_capitals", rushPoints);
                highScoreText.text = LocalizationText.GetText("ui-hscoreCapitals") + rushPoints;
                UIMenu.OnAddScoreToLeaderBoard();
            }
            else
                highScoreText.text = LocalizationText.GetText("ui-bestCapitals") +
                                     PlayerPrefs.GetInt("high_score_capitals", 0);
        }
        else
        {
            if (rushPoints > PlayerPrefs.GetInt("high_score_countries", 0))
            {
                PlayerPrefs.SetInt("high_score_countries", rushPoints);
                highScoreText.text = LocalizationText.GetText("ui-hscoreCountries") + rushPoints;
                UIMenu.OnAddScoreToLeaderBoard();
            }
            else
                highScoreText.text = LocalizationText.GetText("ui-bestCapitals") +
                                     PlayerPrefs.GetInt("high_score_countries", 0);
        }
    }
}