﻿using UnityEngine;

public enum SceneState
{
    Splash, Menu, InitMenu, InitPractice, Game, ZoomIn, ZoomOut,
    Options, InitOptions, InitPlayModes, PlayModes, InitTimeAttack, InitGame,
    TimeAttack
}

public class UIManager : Singleton<UIManager>
{
    protected UIManager() { } // Prevents from using the constructor

    public SceneState currentState;
    public UIMenu menu;
    public UIOptions options;
    public UIPlaymodes playModes;
    public UIGame game;
    public CameraManager cameraManager;
    public UITimer timer;
    public UICard splash;
    public SceneState previousState;
    public CanvasGroup cg;
    public PlayModeSlider playModeSlider;
    public GameObject TimeRushPanel;
    public GameObject PracticePanel;


    void Start()
    {
        currentState = SceneState.Splash;

    }
    void Update()
    {
        switch (currentState)
        {
            case SceneState.Splash: InitSplash(); break;
            case SceneState.Menu: UpdateMenUState(); break;
            case SceneState.ZoomOut: UpdateZoomOutState(); break;
            case SceneState.ZoomIn: UpdateZoomInState(); break;
            case SceneState.Game: UpdateGameState(); break;
            case SceneState.Options: UpdateOptionsState(); break;
            case SceneState.InitOptions: InitOptions(); break;
            case SceneState.PlayModes: UpdatePlayModesState(); break;
            case SceneState.InitPlayModes: InitPlayModesState(); break;
            case SceneState.InitPractice: InitPracticeState(); break;
            case SceneState.InitTimeAttack: InitTimeAttackState(); break;
            case SceneState.InitGame: InitGameState(); break;
            case SceneState.TimeAttack: TimeAttackState(); break;
            case SceneState.InitMenu: InitMenuState(); break;


        }
    }

    private void InitSplash()
    {
        splash.FadeOut(2, 1);
        menu.FadeIn(3f, 1);
        currentState = SceneState.Menu;
    }

    private void InitMenuState()
    {
        playModes.FadeOut(0, 1);
        menu.FadeIn(1, 1);
        currentState = SceneState.Menu;
    }

    private void TimeAttackState()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            timer.FadeOut(0, 1);
            playModes.FadeIn(1, 1);
            currentState = SceneState.InitPlayModes;
        }
    }

    private void InitGameState()
    {
        timer.FadeOut(0, .5f);
        game.FadeIn(0, .5f);
        currentState = SceneState.Game;
        game.StartTimer();
    }

    private void InitTimeAttackState()
    {
        PracticePanel.SetActive(false);
        TimeRushPanel.SetActive(true);
        if (previousState == SceneState.PlayModes)
            playModes.FadeOut(0, 1);
        else
        {
            game.FadeOut(0, 1);
            GameManager.Instance.GetRandomDot();
        }

        timer.FadeIn(1, 1);
        previousState = currentState;
        currentState = SceneState.ZoomIn;
    }

    private void InitPracticeState()
    {
        game.ClearScores();
        PracticePanel.SetActive(true);
        TimeRushPanel.SetActive(false);
        currentState = SceneState.ZoomIn;
        playModes.FadeOut(0, 1);
        game.FadeIn(2, 1);
    }

    private void InitOptions()
    {
        menu.FadeOut(0, 1);
        options.FadeIn(1, 1);
        currentState = SceneState.Options;
    }

    private void InitPlayModesState()
    {
        menu.FadeOut(0, 1);
        playModes.FadeIn(1, 1);
        currentState = SceneState.PlayModes;
        playModeSlider.ChangeMode(playModeSlider.slider.value);
    }

    private void UpdatePlayModesState()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            currentState = SceneState.InitMenu;
        }
    }

    private void UpdateOptionsState()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            FromOptToMenu();
        }
    }
    public void FromOptToMenu()
    {
        GameManager.Instance.ApplyOptions();
        currentState = SceneState.Menu;
        options.FadeOut(0, 1);
        menu.FadeIn(1, 1);
    }
    /// <summary>
    /// Zoom in main camera
    /// </summary>
    private void UpdateZoomInState()
    {
        cameraManager.MoveCam(cameraManager.zoomFov);
    }

    /// <summary>
    /// Checks for input in game state
    /// in order to come back to menu
    /// </summary>
    private void UpdateGameState()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (previousState == SceneState.InitTimeAttack)
                timer.FadeOut(0, 1);
            FromGameToPlaymodes();
        }
    }
    public void FromGameToPlaymodes()
    {

        game.ResetTimer();
        GameManager.Instance.DestroyDot(1);
        currentState = SceneState.ZoomOut;
        game.FadeOut(0, 1);
        playModes.FadeIn(1, 1);
        playModeSlider.ChangeMode(playModeSlider.slider.value);
    }

    /// <summary>
    /// Zoom out main camera
    /// </summary>
    private void UpdateZoomOutState()
    {
        cameraManager.MoveCam(cameraManager.initialFov);
    }

    /// <summary>
    /// Checks for input in menu
    /// in order to quit the app
    /// </summary>
    private void UpdateMenUState()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            menu.Quit();
        }
    }
}
