﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class UIOptions : UICard
{
    private Toggle[] continents;
    private int activeContinents; // the number of active toggles

    void Start()
    {
        continents = canvasGameObject.GetComponentsInChildren<Toggle>();
        LoadPrefs();
        InitializeListeners();
    }

    public void UpdatePrefs(string continentName)
    {
        foreach (Toggle continent in continents)
        {
            if (continent.name == continentName)
            {
                if (activeContinents == 1 && !continent.isOn)
                {
                    continent.onValueChanged.RemoveAllListeners();
                    continent.isOn = true;
                    continent.onValueChanged.AddListener(delegate { UpdatePrefs(continentName); });
                    break;
                }
                else
                {
                    PlayerPrefs.SetInt(continent.name, continent.isOn ? 1 : 0);
                    if (continent.isOn)
                    {
                        activeContinents++;
                    }
                    else
                    {
                        activeContinents--;
                    }
                    break;
                }
            }
        }
    }

    void LoadPrefs()
    {
        int isContinentActive; // 1 if active, 0 otherwise
        foreach (Toggle continent in continents)
        {
            isContinentActive = PlayerPrefs.GetInt(continent.name, 1);
            continent.isOn = isContinentActive == 1 ? true : false;
            activeContinents += isContinentActive;
        }
    }

    void InitializeListeners()
    {
        foreach (Toggle continent in continents)
        {
            string temp = continent.name; //needed for delegate
            continent.onValueChanged.AddListener(delegate { UpdatePrefs(temp); });
        }
    }

    public void Back()
    {
        UIManager.Instance.FromOptToMenu();
    }
}
