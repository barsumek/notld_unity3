﻿using UnityEngine.UI;

public class UIPlaymodes : UICard
{
    public Slider slider;

    public void PlayModePractise()
    {
        GameManager.Instance.isTimeAttack = false;
        GameManager.Instance.isCapitalsMode = slider.value == 0 ? true : false;
        GameManager.Instance.GetRandomDot();
        GameManager.Instance.isTimeAttack = false;
        UIManager.Instance.currentState = SceneState.InitPractice;

    }
    public void PlayModeTimeRush()
    {
        GameManager.Instance.isTimeAttack = true;
        UIManager.Instance.previousState = SceneState.PlayModes;
        GameManager.Instance.isCapitalsMode = slider.value == 0 ? true : false;
        GameManager.Instance.GetRandomDot();
        GameManager.Instance.isTimeAttack = true;
        UIManager.Instance.currentState = SceneState.InitTimeAttack;

    }
    public void PlayModeBack()
    {
        UIManager.Instance.currentState = SceneState.InitMenu;
    }
}
