﻿using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.UI;

public class UIMenu : UICard
{
    public Toggle soundToggle;
    void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

        PlayGamesPlatform.InitializeInstance(config);
        //// recommended for debugging:
        //PlayGamesPlatform.DebugLogEnabled = true;
        //// Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
        LogIn();
        soundToggle.onValueChanged.AddListener(delegate
        {
            if (soundToggle.isOn)
            {
                AudioManager.instance.efxSource.mute = false;
                AudioManager.instance.musicSource.mute = false;
            }

            else
            {
                AudioManager.instance.efxSource.mute = true;
                AudioManager.instance.musicSource.mute = true;
            }
        });
    }

    public void LogIn()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("Login Sucess");
            }
            else
            {
                Debug.Log("Login failed");
            }
        });
    }

    public void PlayModes()
    {
        UIManager.Instance.currentState = SceneState.InitPlayModes;
    }

    public void Options()
    {
        UIManager.Instance.currentState = SceneState.InitOptions;
    }
    public void Quit()
    {
        Application.Quit();
    }

    public void RateUs()
    {
        Application.OpenURL("http://play.google.com/store/apps/details?id=com.nullpointer.globetrotter");
    }

    public void OnShowLeaderBoard()
    {
        //        Social.ShowLeaderboardUI (); // Show all leaderboard
        ((PlayGamesPlatform) Social.Active).ShowLeaderboardUI(); // Show current (Active) leaderboard
    }

    public static void OnAddScoreToLeaderBoard()
    {
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(PlayerPrefs.GetInt("high_score_capitals", 0),
                GooglePlayIDs.leaderboard_globetrotter_capitals_masters, (bool success) =>
                {
                    if (success)
                    {
                        Debug.Log("Update Score Success");
                    }
                    else
                    {
                        Debug.Log("Update Score Fail");
                    }
                });
            Social.ReportScore(PlayerPrefs.GetInt("high_score_countries", 0),
                GooglePlayIDs.leaderboard_globetrotter_countries_masters, (bool success) =>
                {
                    if (success)
                    {
                        Debug.Log("Update Score Success");
                    }
                    else
                    {
                        Debug.Log("Update Score Fail");
                    }
                });
        }
    }
}