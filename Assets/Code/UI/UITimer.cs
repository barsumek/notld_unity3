﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UITimer : UICard
{

    public GameObject startButton;
    void Awake()
    {
        startButton.GetComponent<Button>().onClick.AddListener(() => { OnClick(); });
    }
    void OnClick()
    {
        UIManager.Instance.currentState = SceneState.InitGame;
        UIManager.Instance.game.ClearScores();
    }
}
