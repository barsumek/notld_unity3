﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PlayModeSlider : MonoBehaviour {

    public Slider slider;
    public Image[] images;
    public Color countriesColor;
    public Color capitalsColor;
    public Text text;
    void Start()
    {
        ChangeMode(slider.value);
    }

    public void ChangeMode(float x)
    {
        if (slider.value == 1)
            text.text = LocalizationText.GetText("playModes-countries");
        else if (slider.value == 0)
            text.text = LocalizationText.GetText("playModes-capitals");
        if (images!=null)
        {
            foreach (Image img in images)
            {   
                if (slider.value == 1)
                    img.CrossFadeColor(countriesColor, 1f, false, true);
                else if (slider.value == 0)
                    img.CrossFadeColor(capitalsColor, 1f, false, true);
            }
        }
       
    }
}
