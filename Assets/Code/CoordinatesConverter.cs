﻿using UnityEngine;
using System.Collections;

public class CoordinatesConverter : MonoBehaviour
{
    public Vector3 globeCenter;
    //radius of the globe
    float radius;

    void Start()
    {
        radius = Vector3.Distance(globeCenter, transform.position);
    }

    /// <summary>
    /// Returns a Vector3 with correct globe position calculated from geographical coordinates.
    /// </summary>
    /// <param name="latitude">float, decimal latitute, + for North, - for South</param>
    /// <param name="longitude">float, decimal longtitude, + for East, - for West </param>
    /// <returns></returns>
    public Vector3 ConvertToVec3(float latitude, float longitude)
    {

        var latRad = ConvertDegreesToRadians(latitude);
        var longRad = ConvertDegreesToRadians(longitude);

        float a = radius * Mathf.Cos(latRad);
        var x = a * Mathf.Cos(longRad);
        var y = radius * Mathf.Sin(latRad);
        var z = a * Mathf.Sin(longRad);

        return new Vector3(x, y, z);
    }

    public float ConvertDegreesToRadians(float degrees)
    {
        return degrees * Mathf.PI / 180f;
    }
}
