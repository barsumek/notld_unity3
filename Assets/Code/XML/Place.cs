﻿using System.Xml;
using System.Xml.Serialization;

public class Place
{
    private string capital;
    private string country;
    private float xpos;
    private float ypos;
    private float zpos;
	private int difficulty;

    public string Capital
    {
        get
        {
            return capital;
        }
    }

    public string Country
    {
        get
        {
            return country;
        }
    }

    public float Xpos
    {
        get
        {
            return xpos;
        }
    }

    public float Ypos
    {
        get
        {
            return ypos;
        }
    }

    public float Zpos
    {
        get
        {
            return zpos;
        }
    }

    public int Difficulty
    {
        get
        {
            return difficulty;
        }
    }

	public Place(string country, string capital, float xpos, float ypos, float zpos, int difficulty)
    {
        this.country = country;
        this.capital = capital;
        this.xpos = xpos;
        this.ypos = ypos;
        this.zpos = zpos;
		this.difficulty = difficulty;
    }

}
