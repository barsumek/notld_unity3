﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;

public class PlaceContainer
{
    public static Dictionary<string, List<Place>> Load()
    {
        TextAsset textAsset = (TextAsset)Resources.Load("DataFiles/places" + CheckLanguage.GetLanguage(), typeof(TextAsset));
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(textAsset.text);
        var continents = new Dictionary<string, List<Place>>
        {
            {"Africa", new List<Place>()},
            {"Asia", new List<Place>()},
            {"Australia", new List<Place>()},
            {"Central America", new List<Place>()},
            {"Europe", new List<Place>()},
            {"North America", new List<Place>()},
            {"South America", new List<Place>()}
        };

        XmlNodeList xmlList = xmldoc.GetElementsByTagName("Continent");

        foreach (XmlNode continent in xmlList)
        {
            string name = continent.Attributes["name"].Value;
            XmlNodeList places = continent.ChildNodes;
            foreach (XmlNode place in places)
            {
                string capital = place.Attributes["name"].Value;
                XmlNodeList placeChilds = place.ChildNodes;
                string country = placeChilds[0].InnerText;
                float xpos = float.Parse(placeChilds[1].InnerText);
                float ypos = float.Parse(placeChilds[2].InnerText);
                float zpos = float.Parse(placeChilds[3].InnerText);
                int difficulty = int.Parse(placeChilds[4].InnerText);
                continents[name].Add(new Place(country, capital, xpos, ypos, zpos, difficulty));
            }
        }

        return continents;
    }
}