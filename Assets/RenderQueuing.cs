﻿using UnityEngine;
using System.Collections;

public class RenderQueuing : MonoBehaviour
{
    public Material[] materials;
    public int cloud;
    public int border;
    public int earth;
	// Use this for initialization
	void Start () {
        //Clouds
	    materials[0].renderQueue = cloud;
        //Borders
	    materials[1].renderQueue = border;
        //Earth
	    materials[2].renderQueue = earth;
	}
}
